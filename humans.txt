/* PSEDUCA IT - Plataforma de Servicios Educativos. */
/* Universidad Autónoma del Estado de México. */

/* TEAM */
Fundador: Alejandro González Reyes
Site: https://misitio.com.mx/
Contacto: alejandro.gonzalez[at]figment.com.mx
Twitter: @cuentadesarrollador
Location: Toluca, México

/* THANKS */
http://www.desarrollowebmoderno.br
http://www.amantesdelcodigo.com
@desarrolladorFS

/* SITE */
Last update: 2021/05/10
Language: Español
Standars: HTML5, CSS3, JavaScripts, Responsive Web Design
Components: JQuery, Google Maps, Bootstrap, FontAwesome
Software: Visual Studio Code, Illustrator

/* http://humanstxt.org */